var _ = require('lodash')
  , knex = require('knex')
  , expect = require('expect.js')
  , express = require('express')
  , Promise = require('bluebird')
  , bodyParser = require('body-parser')
  , compression = require('compression')
  , classUtils = require('../../class-utils')
  , findQuery = require('../../database/find-query')
  , SqlModel = require('../../models/SqlModel')
  , request = require('../../http/request');

describe('find-query', function () {
  var db;
  var app;
  var handler;
  var ParentModel;
  var ChildModel;
  var parentModels;
  var childModels;

  before(function () {
    db = knex({
      debug: false,
      client: 'sqlite3',
      connection: {
        filename: ':memory:'
      }
    });
  });

  before(function () {
    return db.schema.createTable('ChildModel', function (t) {
      t.increments('id').primary();
      t.string('prop3');
      t.string('prop4');
    }).createTable('ParentModel', function (t) {
      t.increments('id').primary();
      t.biginteger('rel1Id').references('id').inTable('ChildModel');
      t.biginteger('differentNameOnPurposeId').references('id').inTable('ChildModel');
      t.string('prop1');
      t.string('prop2');
    });
  });

  after(function () {
    return db.schema
      .dropTableIfExists('ChildModel')
      .dropTableIfExists('ParentModel');
  });

  before(function () {
    function _ChildModel() {
      SqlModel.apply(this, arguments);
    }

    function _ParentModel() {
      SqlModel.apply(this, arguments);
    }

    classUtils.inherits(_ChildModel, SqlModel);
    classUtils.inherits(_ParentModel, SqlModel);

    _ChildModel.schema = {
      type: 'object',
      properties: {
        id: {type: ['number', 'null']},
        prop3: {type: 'string'},
        prop4: {type: 'string'}
      }
    };

    _ParentModel.schema = {
      type: 'object',
      properties: {
        id: {type: ['number', 'null']},
        rel1Id: {type: ['number', 'null']},
        differentNameOnPurposeId: {type: ['number', 'null']},
        prop1: {type: 'string'},
        prop2: {type: 'string'}
      }
    };

    _ParentModel.relationMappings = {
      rel1: {
        modelClass: _ChildModel,
        relation: SqlModel.HasOneRelation,
        joinColumn: 'rel1Id'
      },
      rel2: {
        modelClass: _ParentModel,
        relation: SqlModel.HasOneRelation,
        joinColumn: 'differentNameOnPurposeId'
      }
    };

    _ChildModel.tableName = 'ChildModel';
    _ParentModel.tableName = 'ParentModel';

    ChildModel = _ChildModel.bindDb(db);
    ParentModel = _ParentModel.bindDb(db);
  });

  before(function (done) {
    app = express();
    app.use(bodyParser.json());
    app.get('/resource', function (req, res) {
      try {
        handler(req, res).then(function (result) {
          res.json(result);
        }).catch(function (err) {
          res.status(err.statusCode || 500).json(err.data || null);
        });
      } catch (err) {
        res.status(err.statusCode || 500).json(err.data || null);
      }
    });
    app.server = require('http').createServer(app);
    app.server.listen(8088, void 0, void 0, function () {
      done();
    });
  });

  after(function(done) {
    app.server.close(function() {
      done();
    });
  });

  before(function () {
    var childCount = 10;
    var parentCount = 10;

    childModels = _.map(_.range(childCount), function (index) {
      return {
        id: index + 1,
        prop3: 'child value ' + index,
        prop4: 'child value ' + (9 - index)
      };
    });

    parentModels = _.map(_.range(parentCount), function (index) {
      return {
        id: index + 1,
        prop1: 'parent value ' + index,
        prop2: 'parent value ' + (9 - index),
        rel1Id: (index % 2 === 0) ? (index / 2) + 1 : null,
        differentNameOnPurposeId: (index % 2 !== 0) ? ((index - 1) / 2) + 1 : null
      };
    });

    return db(ChildModel.tableName)
      .insert(childModels)
      .then(function () {
        return db(ParentModel.tableName).insert(parentModels);
      });
  });

  describe('explicit join', function () {

    before(function () {
      handler = function (req, res) {
        return findQuery
          .builder(ParentModel, req, res)
          .allowJoin('rel1Prop3', 'rel2Prop1', 'rel2Prop2')
          .build();
      };
    });

    it('should return all when no query parameters or headers are given', function () {
      var expected = _.cloneDeep(parentModels);
      return request
        .get('http://localhost:8088/resource')
        .then(function (res) {
          expect(res.statusCode).to.equal(200);
          expect(res.body).to.eql(expected);
        });
    });

    it('should join a related property defined by `join` query parameter (different table)', function () {
      var expected = _.map(_.cloneDeep(parentModels), function (exp) {
        var rel1 = _.find(childModels, {id: exp.rel1Id});
        exp.rel1Prop3 = rel1 ? rel1.prop3 : null;
        return exp;
      });
      return request
        .get('http://localhost:8088/resource')
        .query('join', 'rel1Prop3')
        .then(function (res) {
          expect(res.statusCode).to.equal(200);
          expect(res.body).to.eql(expected);
        });
    });

    it('should join a related property defined by `join` query parameter (same table)', function () {
      var expected = _.map(_.cloneDeep(parentModels), function (exp) {
        var rel2 = _.find(parentModels, {id: exp.differentNameOnPurposeId});
        exp.rel2Prop1 = rel2 ? rel2.prop1 : null;
        return exp;
      });
      return request
        .get('http://localhost:8088/resource')
        .query('join', 'rel2Prop1')
        .then(function (res) {
          expect(res.statusCode).to.equal(200);
          expect(res.body).to.eql(expected);
        });
    });

    it('should join a related property defined by `join` query parameter (multiple tables and properties)', function () {
      var expected = _.map(_.cloneDeep(parentModels), function (exp) {
        var rel1 = _.find(childModels, {id: exp.rel1Id});
        var rel2 = _.find(parentModels, {id: exp.differentNameOnPurposeId});
        exp.rel1Prop3 = rel1 ? rel1.prop3 : null;
        exp.rel2Prop1 = rel2 ? rel2.prop1 : null;
        exp.rel2Prop2 = rel2 ? rel2.prop2 : null;
        return exp;
      });
      return request
        .get('http://localhost:8088/resource')
        .query('join', 'rel1Prop3')
        .query('join', 'rel2Prop1')
        .query('join', 'rel2Prop2')
        .then(function (res) {
          expect(res.statusCode).to.equal(200);
          expect(res.body).to.eql(expected);
        });
    });

    it('should not be able to join property not listed in `allowJoin` list', function () {
      return request
        .get('http://localhost:8088/resource')
        .query('join', 'rel1Prop4')
        .then(function (res) {
          expect(res.statusCode).to.equal(400);
          expect(res.body.rel1Prop4).to.equal('join not allowed');
        });
    });

    it('should not be able to join nonexistent property', function () {
      return request
        .get('http://localhost:8088/resource')
        .query('join', 'rel1Blaa')
        .then(function (res) {
          expect(res.statusCode).to.equal(400);
          expect(res.body.rel1Blaa).to.equal('invalid property');
        });
    });

  });

  describe('search', function () {

    beforeEach(function () {
      handler = function (req, res) {
        return findQuery
          .builder(ParentModel, req, res)
          .allowJoin('rel1Prop3')
          .allowSearch('prop1', 'prop2', 'rel1Prop3', 'rel2Prop1', 'rel2Prop2')
          .defaultSearch('prop2')
          .build();
      };
    });

    it('should search by default property `prop2`', function () {
      var search = 'value 3';
      var expected = _.filter(_.cloneDeep(parentModels), function (model) {
        return model.prop2.toLowerCase().indexOf(search) !== -1;
      });
      return request
        .get('http://localhost:8088/resource')
        .query('search', '%' + search)
        .then(function (res) {
          expect(res.statusCode).to.equal(200);
          expect(res.body).to.have.length(1);
          expect(res.body).to.eql(expected);
        });
    });

    it('should search by `prop1`', function () {
      var search = 'value 3';
      var expected = _.filter(_.cloneDeep(parentModels), function (model) {
        return model.prop1.toLowerCase().indexOf(search) !== -1;
      });
      return request
        .get('http://localhost:8088/resource')
        .query('search', '%' + search)
        .query('searchBy', 'prop1')
        .then(function (res) {
          expect(res.statusCode).to.equal(200);
          expect(res.body).to.have.length(1);
          expect(res.body).to.eql(expected);
        });
    });

    it('should search by `prop1` and `prop2`', function () {
      var search = 'value 3';
      var expected = _.filter(_.cloneDeep(parentModels), function (model) {
        return model.prop1.toLowerCase().indexOf(search) !== -1
            || model.prop2.toLowerCase().indexOf(search) !== -1;
      });
      return request
        .get('http://localhost:8088/resource')
        .query('search', '%' + search)
        .query('searchBy', 'prop1')
        .query('searchBy', 'prop2')
        .then(function (res) {
          expect(res.statusCode).to.equal(200);
          expect(res.body).to.have.length(2);
          expect(res.body).to.eql(expected);
        });
    });

    it('should search by related property, requiring a join', function () {
      var search = 'value 4';

      var expected = _.map(_.cloneDeep(parentModels), function (exp) {
        var rel1 = _.find(childModels, {id: exp.rel1Id});
        exp.rel1Prop3 = rel1 ? rel1.prop3 : null;
        return exp;
      });

      expected = _.filter(expected, function (model) {
        return model.rel1Prop3 && model.rel1Prop3.toLowerCase().indexOf(search) !== -1;
      });

      return request
        .get('http://localhost:8088/resource')
        .query('search', '%' + search)
        .query('searchBy', 'rel1Prop3')
        .then(function (res) {
          expect(res.statusCode).to.equal(200);
          expect(res.body).to.have.length(1);
          expect(res.body).to.eql(expected);
        });
    });

    it('should not be able to search by property not listed in `allowSearch` list', function () {
      var search = '1';
      return request
        .get('http://localhost:8088/resource')
        .query('search', search)
        .query('searchBy', 'id')
        .then(function (res) {
          expect(res.statusCode).to.equal(400);
          expect(res.body.id).to.equal('search is not allowed by this property');
        });
    });

    it('should not be able to search by nonexistent property', function () {
      var search = '1';
      return request
        .get('http://localhost:8088/resource')
        .query('search', search)
        .query('searchBy', 'blaa')
        .then(function (res) {
          expect(res.statusCode).to.equal(400);
          expect(res.body.blaa).to.equal('search is not allowed by this property');
        });
    });

    it('should not be able to search by related property not listed in `allowJoin` list', function () {
      var search = '1';
      return request
        .get('http://localhost:8088/resource')
        .query('search', search)
        .query('searchBy', 'rel2Prop1')
        .then(function (res) {
          expect(res.statusCode).to.equal(400);
          expect(res.body.rel2Prop1).to.equal('join not allowed');
        });
    });

    it('should use searchMethod if set explicitly', function () {
      handler = function (req, res) {
        return findQuery
          .builder(ParentModel, req, res)
          .allowSearch('prop1', 'prop2')
          // Override the way 'prop1' is searched.
          .searchMethod('prop1', function (queryBuilder, columnName, search) {
            queryBuilder.orWhere(columnName, '>', search);
          })
          .build();
      };

      var search = 'parent value 6';
      var expected = _.filter(_.cloneDeep(parentModels), function (model) {
        return model.prop1.toLowerCase() > search || model.prop2.toLowerCase().indexOf(search) !== -1;
      });

      return request
        .get('http://localhost:8088/resource')
        .query('search', search)
        .query('searchBy', 'prop1')
        .query('searchBy', 'prop2')
        .then(function (res) {
          expect(res.statusCode).to.equal(200);
          expect(res.body).to.have.length(expected.length);
          expect(res.body).to.eql(expected);
        });
    });

  });

  describe('order by', function () {

    before(function () {
      handler = function (req, res) {
        return findQuery
          .builder(ParentModel, req, res)
          .allowJoin('rel1Prop3')
          .allowSearch('rel1Prop3')
          .allowOrderBy('prop1', 'prop2', 'rel1Prop3', 'rel2Prop1', 'rel2Prop2')
          .defaultOrderBy('prop2')
          .build();
      };
    });

    it('should order by default property `prop2`', function () {
      var expected = _.sortBy(_.cloneDeep(parentModels), 'prop2');
      return request
        .get('http://localhost:8088/resource')
        .then(function (res) {
          expect(res.statusCode).to.equal(200);
          expect(res.body).to.eql(expected);
        });
    });

    it('should order by `prop1`', function () {
      var expected = _.sortBy(_.cloneDeep(parentModels), 'prop1');
      return request
        .get('http://localhost:8088/resource')
        .query('orderBy', 'prop1')
        .then(function (res) {
          expect(res.statusCode).to.equal(200);
          expect(res.body).to.eql(expected);
        });
    });

    it('should order by `prop1` in descending order', function () {
      var expected = _.sortBy(_.cloneDeep(parentModels), 'prop1').reverse();
      return request
        .get('http://localhost:8088/resource')
        .query('orderBy', 'prop1')
        .query('orderDesc', true)
        .then(function (res) {
          expect(res.statusCode).to.equal(200);
          expect(res.body).to.eql(expected);
        });
    });

    it('should order by related property requiring a join', function () {
      var expected = _.map(_.cloneDeep(parentModels), function (exp) {
        var rel1 = _.find(childModels, {id: exp.rel1Id});
        exp.rel1Prop3 = rel1 ? rel1.prop3 : null;
        return exp;
      });

      expected = _.filter(expected, 'rel1Prop3');
      expected = _.sortBy(expected, 'rel1Prop3').reverse();

      return request
        .get('http://localhost:8088/resource')
        .query('search', '%value%') // Just to remove nulls.
        .query('searchBy', 'rel1Prop3')
        .query('orderBy', 'rel1Prop3')
        .query('orderDesc', true)
        .then(function (res) {
          expect(res.statusCode).to.equal(200);
          expect(res.body).to.eql(expected);
        });
    });

    it('should not be able to order by property not listed in `allowOrderBy` list', function () {
      return request
        .get('http://localhost:8088/resource')
        .query('orderBy', 'id')
        .then(function (res) {
          expect(res.statusCode).to.equal(400);
          expect(res.body.id).to.equal('ordering by this property is not allowed');
        });
    });

    it('should not be able to order by nonexistent property', function () {
      return request
        .get('http://localhost:8088/resource')
        .query('orderBy', 'blaa')
        .then(function (res) {
          expect(res.statusCode).to.equal(400);
          expect(res.body.blaa).to.equal('ordering by this property is not allowed');
        });
    });

    it('should not be able to order by related property not listed in `allowJoin` list', function () {
      return request
        .get('http://localhost:8088/resource')
        .query('orderBy', 'rel2Prop1')
        .then(function (res) {
          expect(res.statusCode).to.equal(400);
          expect(res.body.rel2Prop1).to.equal('join not allowed');
        });
    });

  });

  describe('filter', function () {

    before(function () {
      handler = function (req, res) {
        return findQuery
          .builder(ParentModel, req, res)
          .allowJoin('rel1Prop3')
          .allowFilter('prop1', 'prop2', 'rel1Prop3', 'rel2Prop1', 'rel2Prop2')
          .build();
      };
    });

    it('should filter by `prop1`', function () {
      var expected = _.where(_.cloneDeep(parentModels), {prop1: 'parent value 8'});
      return request
        .get('http://localhost:8088/resource')
        .query('prop1', 'parent value 8')
        .then(function (res) {
          expect(res.statusCode).to.equal(200);
          expect(res.body).to.eql(expected);
        });
    });

    it('should filter by `prop1` (multiple values)', function () {
      var expected = [
        _.find(_.cloneDeep(parentModels), {prop1: 'parent value 4'}),
        _.find(_.cloneDeep(parentModels), {prop1: 'parent value 5'}),
        _.find(_.cloneDeep(parentModels), {prop1: 'parent value 8'})
      ];
      return request
        .get('http://localhost:8088/resource')
        .query('prop1', 'parent value 8')
        .query('prop1', 'parent value 5')
        .query('prop1', 'parent value 4')
        .then(function (res) {
          expect(res.statusCode).to.equal(200);
          expect(res.body).to.eql(expected);
        });
    });

    it('should filter by `prop1` and `rel1Prop3`', function () {
      var expected = _.where(_.cloneDeep(parentModels), {prop1: 'parent value 8'});
      expected[0].rel1Prop3 = _.find(_.cloneDeep(childModels), {prop3: 'child value 4'}).prop3;
      return request
        .get('http://localhost:8088/resource')
        .query('prop1', 'parent value 8')
        .query('rel1Prop3', 'child value 4')
        .then(function (res) {
          expect(res.statusCode).to.equal(200);
          expect(res.body).to.eql(expected);
        });
    });

    it('should not be able to filter by related property not listed in `allowJoin` list', function () {
      return request
        .get('http://localhost:8088/resource')
        .query('rel2Prop1', 'parent value 1')
        .then(function (res) {
          expect(res.statusCode).to.equal(400);
          expect(res.body.rel2Prop1).to.equal('join not allowed');
        });
    });

  });

  describe('paging', function () {

    before(function () {
      handler = function (req, res) {
        return findQuery
          .builder(ParentModel, req, res)
          .allowPaging(true)
          .build();
      };
    });

    it('should select first page', function () {
      var expected = _.cloneDeep(parentModels).slice(0, parentModels.length / 2);
      return request
        .get('http://localhost:8088/resource')
        .header('range', 'resources=0-' + (parentModels.length / 2 - 1))
        .then(function (res) {
          expect(res.statusCode).to.equal(200);
          expect(res.headers['content-range']).to.equal('resources 0-' + (parentModels.length / 2 - 1) + '/' + parentModels.length);
          expect(res.body).to.eql(expected);
        });
    });

    it('should select second page', function () {
      var expected = _.cloneDeep(parentModels).slice(parentModels.length / 2);
      return request
        .get('http://localhost:8088/resource')
        .header('range', 'resources=' + (parentModels.length / 2) + '-' + (parentModels.length - 1))
        .then(function (res) {
          expect(res.statusCode).to.equal(200);
          expect(res.headers['content-range']).to.equal('resources ' + (parentModels.length / 2) + '-' + (parentModels.length - 1) + '/' + parentModels.length);
          expect(res.body).to.eql(expected);
        });
    });

  });

  describe('eager', function () {

    before(function () {
      handler = function (req, res) {
        return findQuery
          .builder(ParentModel, req, res)
          .allowPaging(true)
          .allowEager('[rel1, rel2.[rel1, rel2]]')
          .build();
      };
    });

    it('should fetch one relation eagerly', function () {

      var expected = _.map(_.cloneDeep(parentModels), function (exp) {
        exp.rel1 = _.find(childModels, {id: exp.rel1Id}) || null;
        return exp;
      });

      return request
        .get('http://localhost:8088/resource')
        .query('eager', 'rel1')
        .then(function (res) {
          expect(res.statusCode).to.equal(200);
          expect(res.body).to.eql(expected);
        });
    });

    it('should fetch two relations eagerly', function () {

      var expected = _.map(_.cloneDeep(parentModels), function (exp) {
        exp.rel1 = _.find(childModels, {id: exp.rel1Id}) || null;
        exp.rel2 = _.find(parentModels, {id: exp.differentNameOnPurposeId}) || null;
        return exp;
      });

      return request
        .get('http://localhost:8088/resource')
        .query('eager', '[rel1, rel2]')
        .then(function (res) {
          expect(res.statusCode).to.equal(200);
          expect(res.body).to.eql(expected);
        });
    });

    it('should fetch nested relations eagerly', function () {

      var expected = _.map(_.cloneDeep(parentModels), function (exp) {
        exp.rel1 = _.find(_.cloneDeep(childModels), {id: exp.rel1Id}) || null;
        exp.rel2 = _.find(_.cloneDeep(parentModels), {id: exp.differentNameOnPurposeId}) || null;
        if (exp.rel2) {
          exp.rel2.rel1 = _.find(_.cloneDeep(childModels), {id: exp.rel2.rel1Id}) || null;
          exp.rel2.rel2 = _.find(_.cloneDeep(parentModels), {id: exp.rel2.differentNameOnPurposeId}) || null;
        }
        return exp;
      });

      return request
        .get('http://localhost:8088/resource')
        .query('eager', '[rel1, rel2.[rel1, rel2]]')
        .then(function (res) {
          expect(res.statusCode).to.equal(200);
          expect(res.body).to.eql(expected);
        });
    });

    it('should not be able to fetch unallowed relations eagerly (1)', function () {
      return request
        .get('http://localhost:8088/resource')
        .query('eager', '[rel1, rel2.*]')
        .then(function (res) {
          expect(res.statusCode).to.equal(400);
          expect(res.body.eager).to.equal('eager query not allowed');
        });
    });

    it('should not be able to fetch unallowed relations eagerly (2)', function () {
      return request
        .get('http://localhost:8088/resource')
        .query('eager', '[rel1.rel2, rel2]')
        .then(function (res) {
          expect(res.statusCode).to.equal(400);
          expect(res.body.eager).to.equal('eager query not allowed');
        });
    });

    it('should not be able to fetch unallowed relations eagerly (3)', function () {
      return request
        .get('http://localhost:8088/resource')
        .query('eager', '[rel1, rel2.[rel1, rel2.rel1]]')
        .then(function (res) {
          expect(res.statusCode).to.equal(400);
          expect(res.body.eager).to.equal('eager query not allowed');
        });
    });

  });

  describe('combination', function () {

    before(function () {
      handler = function (req, res) {
        return findQuery
          .builder(ParentModel, req, res)
          .allowPaging(true)
          .allowJoin('rel1Prop3')
          .allowSearch('rel1Prop3')
          .allowOrderBy('rel1Prop3')
          .defaultOrderBy('rel1Prop3')
          .defaultSearch('rel1Prop3')
          .build();
      };
    });

    it('combination', function () {
      var expected = _.filter(_.cloneDeep(parentModels), 'rel1Id');
      _.each(expected, function (exp) {
        exp.rel1Prop3 = _.find(childModels, {id: exp.rel1Id}).prop3;
      });
      expected = _.sortBy(expected, 'rel1Prop3').reverse().slice(0, 2);
      return request
        .get('http://localhost:8088/resource')
        .query('join', 'rel1Prop3')
        .query('search', '%value%')
        .query('searchBy', 'rel1Prop3')
        .query('orderBy', 'rel1Prop3')
        .query('orderDesc', 'true')
        .header('range', 'resources=0-1')
        .then(function (res) {
          expect(res.statusCode).to.equal(200);
          expect(res.body).to.eql(expected);
        });
    });

  });

});
