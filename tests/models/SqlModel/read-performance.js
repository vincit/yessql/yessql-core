var _ = require('lodash')
  , expect = require('expect.js')
  , uuid = require('uuid')
  , Promise = require('bluebird');

var SqlModel = require('../../../models/SqlModel')
  , classUtils = require('../../../class-utils');

module.exports = function(shared) {

  // Remove comments to enable performance tests.
  /*
  describe('read performance', function() {

    function Model1() {
      SqlModel.call(this);
    }

    classUtils.inherits(Model1, SqlModel);

    Model1.tableName = 'Model';
    Model1.uuidRowIdentifiers = true;

    Model1.relationMappings = {
      hasOne: {
        relation: SqlModel.HasOneRelation,
        modelClass: Model1,
        join: {
          relatedIdColumn: 'hasOneId'
        }
      },
      hasMany: {
        relation: SqlModel.HasManyRelation,
        modelClass: Model1,
        join: {
          ownerIdColumn: 'hasManyId'
        }
      }
    };

    Model1 = Model1.bindDb(shared.db);

    var models = [];

    before(function(done) {
      shared.db.schema.createTable('Model', function(table) {
        table.uuid('id').primary();
        table.uuid('hasOneId').index();
        table.uuid('hasManyId').index();
        table.string('prop1');
        table.float('prop2');
      }).then(function() {
        done();
      }).done();
    });

    after(function(done) {
      shared.db.schema.dropTableIfExists('model').then(function() { done(); }).done();
    });

    it ('should insert 10000', function(done) {
      shared.db.transaction(function(trans) {
        var promises = [];
        for (var i = 0; i < 10000; ++i) {
          var model = Model1.fromJson({
            hasOneId: null,
            hasManyId: null,
            prop1: uuid.v1(),
            prop2: Math.random()
          });
          models.push(model);
          promises.push(model.$insert().transacting(trans));
        }
        return Promise.all(promises);
      }).then(function() {
        done();
      }).done();
    });

    it ('should update 10000', function(done) {
      shared.db.transaction(function(trans) {
        for (var i = 0; i < 1000; ++i) {
          models[i].hasOneId = models[1000 + i].id;
          for (var j = 0; j < 8; ++j) {
            models[2000 + 8 * i + j].hasManyId = models[i].id;
          }
        }
        return Promise.all(_.map(models, function(model) {
          return model.$update().transacting(trans);
        }));
      }).then(function() {
        done();
      }).done();
    });

    it ('should fetch 10000', function(done) {
      Model1.find().then(function(rows) {
        expect(rows.length).to.eql(models.length);
        done();
      }).done();
    });

    it ('should fetch 10000 where 1000 first have 9 children', function(done) {
      Model1.find().eager('hasOne', 'hasMany').then(function(foundModels) {
        expect(foundModels.length).to.eql(models.length);
        var result = true;
        for (var i = 0; i < 1000; ++i) {
          result = result && (foundModels[i].hasOne.id === models[1000 + i].id);
          for (var j = 0; j < 8; ++j) {
            result = result && (foundModels[i].hasMany[j].id === models[2000 + 8 * i + j].id);
          }
        }
        expect(result).to.eql(true);
        done();
      }).done();
    });

    it ('should fetch 10000 (knex)', function(done) {
      Model1.dbQuery().then(function(rows) {
        expect(rows.length).to.eql(models.length);
        done();
      });
    });
  });
  */
};
