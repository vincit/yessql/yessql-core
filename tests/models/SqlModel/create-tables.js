var _ = require('lodash')
  , Promise = require('bluebird');

module.exports = function (shared) {

  before(function () {

    var dropPromises = [
      shared.db.schema.dropTableIfExists('Test1'),
      shared.db.schema.dropTableIfExists('Test2'),
      shared.db.schema.dropTableIfExists('Test3'),
      shared.db.schema.dropTableIfExists('Test1_Test2'),
      shared.db.schema.dropTableIfExists('joinTableForTest2AndTest3')
    ];

    var createModel1Table = shared.db.schema.createTable('Test1', function (t) {
      t.text('id').primary();
      t.text('test2Id');
      t.text('customTest2Id');
      t.text('textProperty1');
      t.integer('numberProperty');
      t.text('jsonProperty');
    });

    var createModel2Table = shared.db.schema.createTable('Test2', function (t) {
      t.text('id').primary();
      t.text('test1Id').index();
      t.text('textProperty2').unique();
    });

    var createModel3Table = shared.db.schema.createTable('Test3', function (t) {
      t.text('id').primary();
      t.text('test2Id').index();
      t.text('textProperty3');
    });

    var createMode1Model2JoinTable = shared.db.schema.createTable('Test1_Test2', function (t) {
      t.text('test1Id').index();
      t.text('test2Id').index();
    });

    var createMode2Model3JoinTable = shared.db.schema.createTable('joinTableForTest2AndTest3', function (t) {
      t.text('thisIsTest2Id').index();
      t.text('andThisIsTest3Id').index();
    });

    var createPromises = [
      createModel1Table,
      createModel2Table,
      createModel3Table,
      createMode1Model2JoinTable,
      createMode2Model3JoinTable
    ];

    return Promise.all(dropPromises).then(function () {
      return Promise.all(createPromises);
    });

  });

  after(function () {

    var promises = [
      shared.db.schema.dropTableIfExists('Test1'),
      shared.db.schema.dropTableIfExists('Test2'),
      shared.db.schema.dropTableIfExists('Test3'),
      shared.db.schema.dropTableIfExists('Test1_Test2'),
      shared.db.schema.dropTableIfExists('joinTableForTest2AndTest3')
    ];

    return Promise.all(promises);

  });

};
