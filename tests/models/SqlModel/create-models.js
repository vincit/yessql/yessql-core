var _ = require('lodash')
  , SqlModel = require('../../../models/SqlModel')
  , classUtils = require('../../../class-utils');

module.exports = function (shared) {

  // All the SqlModel tests work with this graph.
  shared.json = {
    textProperty1: 'text1',
    numberProperty: 1,
    test2Id: null,
    customTest2Id: null,
    jsonProperty: {
      value: 10
    },

    // Class: Model2
    child: {
      textProperty2: 'text2',
      test1Id: null,
      grandChildren1: [],
      grandChildren2: []
    },

    // Class: Model2
    customChild: {
      textProperty2: 'text3',
      test1Id: null,
      grandChildren1: [],
      grandChildren2: []
    },

    // Class: Model2
    children1: [
      {
        textProperty2: 'text4',
        test1Id: null,
        grandChildren1: [],
        grandChildren2: []
      },
      {
        textProperty2: 'text5',
        test1Id: null,
        grandChildren1: [],
        grandChildren2: []
      }
    ],

    // Class: Model2
    children1WithQuery: [
      {
        textProperty2: 'text4',
        test1Id: null,
        grandChildren1: [],
        grandChildren2: []
      }
    ],

    // Class: Model2
    children2: [
      {
        textProperty2: 'text6',
        test1Id: null,

        // Class: Model3
        grandChildren1: [
          {
            textProperty3: 'text7',
            test2Id: null
          }
        ],

        // Class: Model3
        grandChildren2: [
          {
            textProperty3: 'text8',
            test2Id: null
          }
        ]
      },
      {
        textProperty2: 'text9',
        test1Id: null,

        // Class: Model3
        grandChildren1: [
          {
            textProperty3: 'text10',
            test2Id: null
          }
        ],

        // Class: Model3
        grandChildren2: [
          {
            textProperty3: 'text11',
            test2Id: null
          },
          {
            textProperty3: 'text12',
            test2Id: null
          },
          {
            textProperty3: 'text13',
            test2Id: null
          }
        ],

        // Class: Model3
        grandChildren2WithQuery: [
          {
            textProperty3: 'text12',
            test2Id: null
          },
          {
            textProperty3: 'text13',
            test2Id: null
          }
        ]
      }
    ]
  };

  shared.modelClasses = createModelClasses(shared);
  shared.models = createModels(shared);
};

function createModelClasses(shared) {

  /**
   * @extends SqlModel
   * @constructor
   */
  function Model1() {
    SqlModel.call(this);
  }

  /**
   * @extends SqlModel
   * @constructor
   */
  function Model2() {
    SqlModel.call(this);
  }

  /**
   * @extends SqlModel
   * @constructor
   */
  function Model3() {
    SqlModel.call(this);
  }

  classUtils.inherits(Model1, SqlModel);
  classUtils.inherits(Model2, SqlModel);
  classUtils.inherits(Model3, SqlModel);

  Model1.tableName = 'Test1';
  Model1.uuidRowIdentifiers = true;
  Model1.jsonAttributes = ['jsonProperty'];

  Model2.tableName = 'Test2';
  Model2.uuidRowIdentifiers = true;

  Model3.tableName = 'Test3';
  Model3.uuidRowIdentifiers = true;

  Model2.relationMappings = function () {
    return {
      grandChildren1: {
        relation: SqlModel.HasManyRelation,
        modelClass: Model3
      },
      // Custom join table and columns for grandChildren2 relation.
      grandChildren2: {
        relation: SqlModel.ManyToManyRelation,
        modelClass: Model3,
        join: {
          table: 'joinTableForTest2AndTest3',
          ownerIdColumn: 'thisIsTest2Id',
          relatedIdColumn: 'andThisIsTest3Id'
        }
      },
      // Like grandChildren2 but with custom query.
      grandChildren2WithQuery: {
        relation: SqlModel.ManyToManyRelation,
        modelClass: Model3,
        join: {
          table: 'joinTableForTest2AndTest3',
          ownerIdColumn: 'thisIsTest2Id',
          relatedIdColumn: 'andThisIsTest3Id'
        },
        query: function (builder) {
          builder.where('textProperty3', '>=', 'text12');
        }
      }
    }
  };

  Model1.relationMappings = {
    child: {
      relation: SqlModel.HasOneRelation,
      modelClass: Model2
    },
    customChild: {
      relation: SqlModel.HasOneRelation,
      modelClass: Model2,
      joinColumn: 'customTest2Id'
    },
    children1: {
      relation: SqlModel.HasManyRelation,
      modelClass: Model2
    },
    children1WithQuery: {
      relation: SqlModel.HasManyRelation,
      modelClass: Model2,
      query: {
        textProperty2: 'text4'
      }
    },
    children2: {
      relation: SqlModel.ManyToManyRelation,
      modelClass: Model2
    }
  };

  Model1.schema = {
    type: 'object',
    required: ['textProperty1', 'numberProperty'],
    properties: {
      id: {type: ['string', 'null']},
      textProperty1: {type: 'string'},
      numberProperty: {type: 'number'},
      test2Id: {type: ['number', 'string', 'null']},
      customTest2Id: {type: ['number', 'string', 'null']},
      jsonProperty: {type: ['object', 'array', 'null']}
    }
  };

  return {
    Model1: Model1.bindDb(shared.db),
    Model2: Model2.bindDb(shared.db),
    Model3: Model3.bindDb(shared.db)
  };
}

function createModels(shared) {
  var json = shared.json;
  var classes = shared.modelClasses;

  var models = {
    model1: [
      classes.Model1.fromJson(omitRelations(json))
    ],
    model2: [
      classes.Model2.fromJson(omitRelations(json.child)),
      classes.Model2.fromJson(omitRelations(json.customChild)),
      classes.Model2.fromJson(omitRelations(json.children1[0])),
      classes.Model2.fromJson(omitRelations(json.children1[1])),
      classes.Model2.fromJson(omitRelations(json.children2[0])),
      classes.Model2.fromJson(omitRelations(json.children2[1]))
    ],
    model3: [
      classes.Model3.fromDatabaseJson(omitRelations(json.children2[0].grandChildren1[0])),
      classes.Model3.fromDatabaseJson(omitRelations(json.children2[0].grandChildren2[0])),
      classes.Model3.fromDatabaseJson(omitRelations(json.children2[1].grandChildren1[0])),
      classes.Model3.fromDatabaseJson(omitRelations(json.children2[1].grandChildren2[0])),
      classes.Model3.fromDatabaseJson(omitRelations(json.children2[1].grandChildren2[1])),
      classes.Model3.fromDatabaseJson(omitRelations(json.children2[1].grandChildren2[2]))
    ]
  };

  return models;
}

function omitRelations(json) {
  // Helper that omits relations from the json objects in shared.json.
  return _.omit(json, ['child', 'customChild', 'children1', 'children1WithQuery', 'children2', 'grandChildren1', 'grandChildren2', 'grandChildren2WithQuery']);
}
