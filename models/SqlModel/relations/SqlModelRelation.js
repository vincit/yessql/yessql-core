"use strict";

var _ = require('lodash')
  , SqlModelQueryBuilder = require('../SqlModelQueryBuilder');

/**
 * Represents a relation between two `SqlModel` subclasses.
 *
 * @param {String} relationName
 *    Name of the relation.
 *
 * @param {SqlModelRelationMapping} mapping
 *    Parameters for this relation.
 *
 * @param {object} OwnerClass
 *    The SqlModel subclass that owns this relation.
 *
 * @constructor
 */
function SqlModelRelation(relationName, mapping, OwnerClass) {
  var join = mapping.join || {}
    , additionalQuery = null
    , RelationClass = this.constructor;

  // joinColumn overrides ownerIdColumn and relatedIdColumn.
  if (mapping.joinColumn) {
    join.ownerIdColumn = mapping.joinColumn;
    join.relatedIdColumn = mapping.joinColumn;
  }

  // If no join information is given, use the defaults.
  join = _.defaults(join, {
    table: RelationClass.joinTableName(OwnerClass, mapping.modelClass),
    ownerIdColumn: RelationClass.joinColumnName(OwnerClass),
    relatedIdColumn: RelationClass.joinColumnName(mapping.modelClass)
  });

  if (mapping.query) {
    if (_.isFunction(mapping.query)) {
      additionalQuery = mapping.query;
    } else {
      additionalQuery = function (queryBuilder) {
        queryBuilder.where(mapping.query);
      };
    }
  }

  /**
   * Name of the relation.
   *
   * @type {String}
   */
  this.name = relationName;

  /**
   * The mapping from which this relation was constructed.
   *
   * @type {SqlModelRelationMapping}
   */
  this.mapping = mapping;

  /**
   * The owner class of this relation.
   *
   * This must be a subclass of SqlModel.
   *
   * @type {object}
   */
  this.ownerModelClass = OwnerClass;

  /**
   * The related class.
   *
   * This must be a subclass of SqlModel.
   *
   * @type {object}
   */
  this.relatedModelClass = mapping.modelClass;

  /**
   * Name of the database column through which ownerModelClass is joined.
   *
   * @type {String}
   */
  this.ownerJoinColumn = join.ownerIdColumn;

  /**
   * Name of the database column through which relatedModelClass is joined.
   *
   * @type {String}
   */
  this.relatedJoinColumn = join.relatedIdColumn;

  /**
   * Join table name.
   *
   * @type {String}
   */
  this.joinTable = join.table;

  /**
   * Optional additional query.
   *
   * @type {function (SqlModelQueryBuilder)}
   */
  this.additionalQuery = additionalQuery;
}

/**
 * Returns a copy of this relation.
 *
 * @returns {SqlModelRelation}
 */
SqlModelRelation.prototype.clone = function () {
  // noinspection JSValidateTypes
  return new this.constructor(this.name, this.mapping, this.ownerModelClass);
};

/**
 * Find models related through this relation.
 *
 * Stores the models to model[this.name].
 *
 * @param {SqlModel} model
 *    The model that owns this relation.
 *
 * @return {SqlModelQueryBuilder}
 */
SqlModelRelation.prototype.find = function (model) {
  // Do nothing by default.
};

/**
 * Find models related through this relation for multiple models at a time.
 *
 * Stores the models to model[this.name] of each model.
 *
 * @param {Array.<SqlModel>} models
 *    The models for which to fetch the relation.
 *
 * @return {SqlModelQueryBuilder}
 */
SqlModelRelation.prototype.findForMany = function (models) {
  var self = this;
  // Naive default implementation just fetches all with separate queries.
  return SqlModelQueryBuilder
    .all(_.map(models, function (model) {
      return self.find(model);
    }));
};

/**
 * Find one model related through this relation.
 *
 * Stores the model to model[this.name].
 *
 * @param {SqlModel} model
 *    The model that owns this relation.
 *
 * @return {SqlModelQueryBuilder}
 */
SqlModelRelation.prototype.findOne = function (model) {
  return this.find(model).runAfterExecution(_.first);
};

/**
 * Update a related model (modelToUpdate).
 *
 * The update model is stored to model[this.name].
 *
 * @param {SqlModel} model
 *    The model that owns this relation.
 *
 * @param {SqlModel} modelToUpdate
 *    Model to update.
 *
 * @return {SqlModelQueryBuilder}
 */
SqlModelRelation.prototype.update = function (model, modelToUpdate) {
  var self = this;
  return modelToUpdate.$update().runAfterExecution(function (updatedModel) {
    model[self.name] = model[self.name] || [];
    var index = _.findIndex(model[self.name], {id: updatedModel.id});
    if (index !== -1) {
      model[self.name][index] = updatedModel;
    } else {
      model[self.name].push(updatedModel);
    }
    return updatedModel;
  });
};

/**
 * Insert a related model (modelToInsert) to database and bind it to model using this relation.
 *
 * The inserted model is stored to model[this.name].
 *
 * @param {SqlModel} model
 *    The model that owns this relation.
 *
 * @param {SqlModel} modelToInsert
 *    Model to insert for the owner through this relation.
 *
 * @return {SqlModelQueryBuilder}
 */
SqlModelRelation.prototype.insert = function (model, modelToInsert) {
  // Do nothing by default.
};

/**
 * Deletes related model from the database and remove the binding to it from the owner model.
 *
 * The removed model is removed from `model[this.name]`.
 *
 * @param {SqlModel} model
 *    The model that owns this relation.
 *
 * @param {Number|String} idOfModelToDelete
 *    Identifier of the related model to delete.
 *
 * @return {SqlModelQueryBuilder}
 */
SqlModelRelation.prototype.del = function (model, idOfModelToDelete) {
  throw new Error('SqlModelRelation: del not implemented');
};

/**
 * Bind an existing model `idOfModelToBind` to `model` using this relation.
 *
 * The bound model is stored to `model[this.name]`.
 *
 * @param {SqlModel} model
 *    The model that owns this relation.
 *
 * @param {Number|String} idOfModelToBind
 *    Identifier of the model to bind.
 *
 * @return {SqlModelQueryBuilder}
 */
SqlModelRelation.prototype.bind = function (model, idOfModelToBind) {
  throw new Error('SqlModelRelation: bind not implemented');
};

/**
 * Unbind an existing model `idOfModelToUnBind` from `model` using.
 *
 * The unbound model is removed from `model[this.name]`.
 *
 * @param {SqlModel} model
 *    The model that owns this relation.
 *
 * @param {Number|String} idOfModelToUnBind
 *    Identifier of the model to unbind.
 *
 * @return {SqlModelQueryBuilder}
 */
SqlModelRelation.prototype.unbind = function (model, idOfModelToUnBind) {
  throw new Error('SqlModelRelation: unbind not implemented');
};

/**
 * Returns a join table row object that joins ´ownerId´ to ´relatedId´.
 *
 * @param {Number|String} ownerId
 * @param {Number|String} relatedId
 * @return {Object}
 */
SqlModelRelation.prototype.joinRow = function (ownerId, relatedId) {
  return _.object([this.ownerJoinColumn, this.relatedJoinColumn], [ownerId, relatedId]);
};

/**
 * Name of the default join column for an SqlModel subclass.
 *
 * @param {object} ModelClass
 * @return {String}
 */
SqlModelRelation.joinColumnName = function (ModelClass) {
  var table = ModelClass.tableName;
  return table.charAt(0).toLowerCase() + table.substring(1) + 'Id';
};

/**
 * Name of the join table between OwnerModelClass and TargetModelClass.
 *
 * @param {object} OwnerModelClass
 * @param {object} TargetModelClass
 * @return {String}
 */
SqlModelRelation.joinTableName = function (OwnerModelClass, TargetModelClass) {
  var table1 = OwnerModelClass.tableName;
  var table2 = TargetModelClass.tableName;
  return (table1 < table2) ? table1 + '_' + table2 : table2 + '_' + table1;
};

module.exports = SqlModelRelation;
