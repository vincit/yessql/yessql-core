"use strict";

var _ = require('lodash')
  , Promise = require('bluebird')
  , EagerExpression = require('./EagerExpression');

/**
 * Class that builds and executes database queries for `SqlModels`.
 *
 * This class has all the methods of a *knex.js* query builder. Instead of returning plain json objects
 * this class returns `SqlModel` instances. The query is executed and converted into a Promise after a call
 * to `then()`, `map()`, `bind()`, `catch()`, `return()`, `range()` or `page()` method.
 *
 * `SqlModelQueryBuilder` also provides a mechanism for fetching the query result models eagerly with
 * relations. See `eager()` method for more info.
 *
 * Examples:
 *
 * ```js
 * SqlModelQueryBuilder.forClass(Person)
 *   .select('name', 'lastName')
 *   .where('age', '>=', '45')
 *   .andWhere('age', '<', '65')
 *   .orWhere('middleAged', true)
 *   .map(function (middleAgedPerson) {
 *     console.log(middleAgedPerson.name, "is middle aged");
 *   });
 *
 * SqlModelQueryBuilder.forClass(Person)
 *   .where('name', 'Sam')
 *   .eager('children', 'father')
 *   .then(function (models) {
 *     console.log("Sam's father's name", models[0].father.name);
 *     console.log("Sam has", models[0].children.length, "children");
 *   });
 * ```
 *
 * @constructor
 */
function SqlModelQueryBuilder() {
  /**
   * @private
   * @type {Object}
   */
  this.modelClass_ = null;

  /**
   * @private
   * @type {QueryBuilder}
   */
  this.knexQueryBuilder_ = null;

  /**
   * @private
   * @type {Formatter}
   */
  this.knexFormatter_ = null;

  /**
   * @private
   * @type {Transaction}
   */
  this.transaction_ = null;

  /**
   * @private
   * @type {Array.<SqlModelQueryBuilder>}
   */
  this.nestedBuilders_ = null;

  /**
   * @private
   * @type {Object|Array.<String>|EagerType}
   */
  this.eager_ = null;

  /**
   * @private
   * @type {{promise:Promise, resolve:function, reject:function}}
   */
  this.runBeforeExecutionPromise_ = this.createPendingPromise_();

  /**
   * @private
   * @type {{promise:Promise, resolve:function, reject:function}}
   */
  this.runAfterQueryExecutionPromise_ = null;

  /**
   * @private
   * @type {{promise:Promise, resolve:function, reject:function}}
   */
  this.runAfterExecutionPromise_ = null;
}

/**
 * Eager fetch type.
 *
 * There is a shortcut to this enum in `SqlModel.EagerType`.
 *
 * @enum {Number}
 */
SqlModelQueryBuilder.EagerType = {
  /**
   * Fetch nothing.
   */
  None: 0,
  /**
   * Fetch one relation recursively (meaningless in the root).
   */
  Recursive: 1,
  /**
   * Fetch all relations recursively.
   */
  AllRecursive: 2
};

/**
 * Specifies the relations that should be fetched eagerly for the result models.
 *
 * Examples:
 *
 * ```js
 * // Fetch all relations recursively.
 * SqlModelQueryBuilder
 *   .forClass(Person)
 *   .where('name', 'Ulf')
 *   .eager('*')
 *   .then(function (models) {
 *     var ulf = models[0];
 *   });
 *
 * // Fetch list of relations.
 * SqlModelQueryBuilder
 *   .forClass(Person)
 *   .where('name', 'Ulf')
 *   .eager('[children, father]')
 *   .then(function (models) {
 *     var ulf = models[0];
 *     var ulfsFirstChild = ulf.children[0];
 *   });
 *
 * // Fetch two levels of relations.
 * SqlModelQueryBuilder
 *   .forClass(Person)
 *   .where('name', 'Ulf')
 *   .eager('father.mother')
 *   .then(function (models) {
 *     var ulf = models[0];
 *     var ulfsGrandMother = ulf.father.mother;
 *   });
 *
 * // Fetch two levels of relations.
 * SqlModelQueryBuilder
 *   .forClass(Person)
 *   .where('name', 'Ulf')
 *   .eager('father.[mother, father]')
 *   .then(function (models) {
 *     var ulf = models[0];
 *     var ulfsGrandMother = ulf.father.mother;
 *     var ulfsGrandFather = ulf.father.father;
 *   });
 *
 * // Fetch three levels of relations. mother's siblings' children
 * // are fetched recursively.
 * SqlModelQueryBuilder
 *   .forClass(Person)
 *   .where('name', 'Ulf')
 *   .eager('[father.[mother, father], mother.siblings.children.^]')
 *   .then(function (models) {
 *     var ulf = models[0];
 *     var cousinsFromMothersSide = [];
 *     _.each(ulf.mother.siblings, function (sibling) {
 *       cousinsFromMothersSide.concat(siblings.children);
 *     });
 *   });
 * ```
 *
 * Also a more verbose object notation is supported:
 *
 * ```js
 * // Fetch all relations recursively.
 * SqlModelQueryBuilder
 *   .forClass(Person)
 *   .where('name', 'Ulf')
 *   .eager(SqlModel.EagerType.AllRecursive)
 *   .then(function (models) {
 *     var ulf = models[0];
 *   });
 *
 * // Fetch list of relations.
 * SqlModelQueryBuilder
 *   .forClass(Person)
 *   .where('name', 'Ulf')
 *   .eager('children', 'father')
 *   .then(function (models) {
 *     var ulf = models[0];
 *     var ulfsFirstChild = ulf.children[0];
 *   });
 *
 * // Fetch two levels of relations.
 * SqlModelQueryBuilder
 *   .forClass(Person)
 *   .where('name', 'Ulf')
 *   .eager({father: 'mother'})
 *   .then(function (models) {
 *     var ulf = models[0];
 *     var ulfsGrandMother = ulf.father.mother;
 *   });
 *
 * // Fetch two levels of relations.
 * SqlModelQueryBuilder
 *   .forClass(Person)
 *   .where('name', 'Ulf')
 *   .eager({father: ['mother', 'father']})
 *   .then(function (models) {
 *     var ulf = models[0];
 *     var ulfsGrandMother = ulf.father.mother;
 *     var ulfsGrandFather = ulf.father.father;
 *   });
 *
 * // Fetch three levels of relations. mother's siblings' children
 * // are fetched recursively.
 * SqlModelQueryBuilder
 *   .forClass(Person)
 *   .where('name', 'Ulf')
 *   .eager({
 *     father: ['mother', 'father'],
 *     mother: {
 *       siblings: {
 *         children: SqlModel.EagerType.Recursive
 *       }
 *     }
 *   })
 *   .then(function (models) {
 *     var ulf = models[0];
 *     var cousinsFromMothersSide = [];
 *     _.each(ulf.mother.siblings, function (sibling) {
 *       cousinsFromMothersSide.concat(siblings.children);
 *     });
 *   });
 * ```
 *
 * @param {Function|Object|String|Array.<String>|EagerType} eager
 * @returns {SqlModelQueryBuilder}
 */
SqlModelQueryBuilder.prototype.eager = function (eager) {
  if (_.isFunction(eager)) {
    return this.eager(eager());
  }
  if (arguments.length <= 1) {
    if (_.isString(eager)) {
      this.eager_ = new EagerExpression(eager).obj;
    } else {
      this.eager_ = eager || null;
    }
  } else {
    this.eager_ = [];
    // Multiple arguments is equal to passing an array.
    for (var i = 0, l = arguments.length; i < l; ++i) {
      this.eager_.push(arguments[i]);
    }
  }
  return this;
};

/**
 * Attaches this query to a transaction.
 *
 * @param {Transaction} transaction
 * @returns {SqlModelQueryBuilder}
 */
SqlModelQueryBuilder.prototype.transacting = function (transaction) {
  this.transaction_ = transaction;

  if (this.nestedBuilders_) {
    _.each(this.nestedBuilders_, function (nestedBuilder) {
      nestedBuilder.transacting(transaction);
    });
  } else {
    this.knexQueryBuilder_.transacting(transaction);
  }

  return this;
};

/**
 * Getter for transaction.
 *
 * @returns {Transaction}
 */
SqlModelQueryBuilder.prototype.transaction = function () {
  return this.transaction_;
};

/**
 * Adds a pending `then(func)` call before the query is executed.
 *
 * Calling this doesn't execute the query. Instead all functions registered using this
 * method are executed just before the query is executed.
 *
 * @param {function} func
 * @returns {SqlModelQueryBuilder}
 */
SqlModelQueryBuilder.prototype.runBeforeExecution = function (func) {
  this.runBeforeExecutionPromise_ = this.runBeforeExecutionPromise_ || this.createPendingPromise_();
  this.runBeforeExecutionPromise_.promise = this.runBeforeExecutionPromise_.promise.then(func);
  return this;
};

/**
 * Adds a pending `then(func)` call just after the query is executed and before any other operations.
 *
 * Calling this doesn't execute the query. Instead all functions registered using this
 * method are executed just after the query is executed and just before any other
 * operations.
 *
 * @param {function} func
 * @returns {SqlModelQueryBuilder}
 */
SqlModelQueryBuilder.prototype.runAfterQueryExecution = function (func) {
  this.runAfterQueryExecutionPromise_ = this.runAfterQueryExecutionPromise_ || this.createPendingPromise_();
  this.runAfterQueryExecutionPromise_.promise = this.runAfterQueryExecutionPromise_.promise.then(func);
  return this;
};

/**
 * Adds a pending `then(func)` call.
 *
 * This is equal to calling `this.then(func)`, but doesn't execute the query.
 *
 * @param {function(Array.<SqlModel>)} func
 * @returns {SqlModelQueryBuilder}
 */
SqlModelQueryBuilder.prototype.runAfterExecution = function (func) {
  this.runAfterExecutionPromise_ = this.runAfterExecutionPromise_ || this.createPendingPromise_();
  this.runAfterExecutionPromise_.promise = this.runAfterExecutionPromise_.promise.then(func);
  return this;
};

/**
 * Executes the builder right after this one and attaches it to the same transaction.
 *
 * @param {function(*):SqlModelQueryBuilder} builderGenerator
 * @returns {SqlModelQueryBuilder}
 */
SqlModelQueryBuilder.prototype.enqueueBuilder = function (builderGenerator) {
  var self = this;
  this.runAfterExecutionPromise_ = this.runAfterExecutionPromise_ || this.createPendingPromise_();
  this.runAfterExecutionPromise_.promise = this.runAfterExecutionPromise_.promise.then(function (result) {
    return builderGenerator(result).transacting(self.transaction_);
  });
  return this;
};

/**
 * Executes this query and returns a promise.
 *
 * Calls `then(func)` for the returned promise.
 *
 * @returns {Promise}
 */
SqlModelQueryBuilder.prototype.then = function (func, errFunc) {
  return this.execute_().then(func, errFunc);
};

/**
 * Executes this query and returns a promise.
 *
 * Calls `map(func)` for the returned promise.
 *
 * @returns {Promise}
 */
SqlModelQueryBuilder.prototype.map = function (func) {
  return this.execute_().map(func);
};

/**
 * Executes this query and returns a promise.
 *
 * Calls `bind(obj)` for the returned promise.
 *
 * @returns {Promise}
 */
SqlModelQueryBuilder.prototype.bind = function (obj) {
  return this.execute_().bind(obj);
};

/**
 * Executes this query and returns a promise.
 *
 * Calls `catch(func)` for the returned promise.
 *
 * @returns {Promise}
 */
SqlModelQueryBuilder.prototype.catch = function (func) {
  return this.execute_().catch(func);
};

/**
 * Executes this query and returns a promise.
 *
 * Calls `return(value)` for the returned promise.
 *
 * @returns {Promise}
 */
SqlModelQueryBuilder.prototype.return = function (value) {
  return this.execute_().return(value);
};

/**
 * Executes a count query that returns just the row count of the query result.
 *
 * The query can still be executed after this method is called.
 *
 * @returns {Promise}
 */
SqlModelQueryBuilder.prototype.resultSize = function () {
  if (!this.nestedBuilders_) {
     var db = this.modelClass_.db;
    // as long as user hasn't select()ed anything explicitly, we can enable the database engine to optimize
    // the count query more by selecting just a fixed value from each row, allowing it to drop e.g. computed
    // column values from expensive subqueries, and use index-only scans more
    var rawQuery = db.raw(this.knexQueryBuilder_.clone().select(1)).wrap('(', ') as temp');
    var countQuery = db.count('* as count').from(rawQuery);
    return countQuery.transacting(this.transaction_).then(function (result) {
      return result[0] ? result[0].count : 0;
    });
  } else {
    throw new Error('resultSize() cannot be called for a nested query');
  }
};

/**
 * Request page from query with total results.
 *
 * Promise returns { total : Number , results : Array.<SqlModel> }
 *
 * ```js
 * SqlModelQueryBuilder
 *   .forClass(Person)
 *   .where('age', '<', 20)
 *   .orderBy('name')
 *   .page(0, 10)
 *   .then(function (page) {
 *     console.log('there are ' + page.total + ' teenagers in total');
 *     return page.results;
 *   });
 * ```
 *
 * @note This executes the query and returns a `Promise` instead of an `SqlModelQueryBuilder`.
 *
 * @returns {Promise}
 */
SqlModelQueryBuilder.prototype.page = function (page, pageSize) {
  return this.range(page * pageSize, (page + 1) * pageSize - 1);
};

/**
 * Request range from query with total results.
 *
 * Promise returns { total : Number , results : Array.<SqlModel> }
 *
 * ```js
 * SqlModelQueryBuilder
 *   .forClass(Person)
 *   .where('age', '<', 20)
 *   .orderBy('name')
 *   .range(100, 120)
 *   .then(function (range) {
 *     console.log('there are ' + range.total + ' teenagers in total');
 *     return range.results;
 *   });
 * ```
 *
 * @note This executes the query and returns a `Promise` instead of an `SqlModelQueryBuilder`.
 *
 * @param {Number} start
 *    Index of first row to fetch (inclusive).
 *
 * @param {Number} end
 *    Index of last row to fetch (inclusive).
 *
 * @returns {Promise}
 */
SqlModelQueryBuilder.prototype.range = function (start, end) {
  if (!this.nestedBuilders_) {
    return Promise.all([
      this.resultSize(),
      this.limit(end - start + 1).offset(start)
    ]).spread(function (total, results) {
      return {
        results: results,
        total: total
      };
    });
  } else {
    throw new Error('range() cannot be called for a nested query');
  }
};

/**
 * Prints the SQL this query builder will generate.
 *
 * @returns {SqlModelQueryBuilder}
 */
SqlModelQueryBuilder.prototype.dumpSql = function () {
  if (!this.nestedBuilders_) {
    console.log(this.knexQueryBuilder_.toString());
  } else {
    _.each(this.nestedBuilders_, function (builder) {
      builder.dumpSql();
    });
  }
  return this;
};

/**
 * Returns the SQL string of this query.
 *
 * @returns {string}
 */
SqlModelQueryBuilder.prototype.toString = function () {
  if (!this.nestedBuilders_) {
    return this.knexQueryBuilder_.toString();
  } else {
    var str = [];

    _.each(this.nestedBuilders_, function (builder) {
      str.push(builder.toString());
    });

    return str.join('\n');
  }
};

/**
 * Simply calls `queryFunc(this)` and returns `this` for chaining.
 *
 * @param {function(SqlModelQueryBuilder)} queryFunc
 * @returns {SqlModelQueryBuilder}
 */
SqlModelQueryBuilder.prototype.runFunction = function (queryFunc) {
  if (queryFunc) {
    queryFunc(this);
  }
  return this;
};

/**
 * Creates a knex.js `Formatter` instance for the connected database.
 *
 * @returns {Formatter}
 */
SqlModelQueryBuilder.prototype.formatter = function () {
  if (!this.knexFormatter_) {
    this.knexFormatter_ = this.modelClass_.dbFormatter();
  }
  return this.knexFormatter_;
};

/**
 * @private
 * @return {{promise:Promise, resolve:function, reject:function}}
 */
SqlModelQueryBuilder.prototype.createPendingPromise_ = function () {
  var pending = {promise: null, resolve: null, reject: null};
  pending.promise = new Promise(function (resolver, rejector) {
    pending.resolve = resolver;
    pending.reject = rejector;
  });
  return pending;
};

/**
 * @private
 * @returns {Promise}
 */
SqlModelQueryBuilder.prototype.execute_ = function () {
  var promise = this.runBeforeExecutionPromise_.promise;

  promise = this.executeQuery_(promise);
  promise = this.executePending_(this.runAfterQueryExecutionPromise_, promise);
  promise = this.executeFetchRelated_(promise);
  promise = this.executePending_(this.runAfterExecutionPromise_, promise);

  // This executes the whole chain by resolving the first chained promise.
  this.runBeforeExecutionPromise_.resolve();

  return promise;
};

/**
 * @private
 * @param {Promise} promise
 * @returns {Promise}
 */
SqlModelQueryBuilder.prototype.executeQuery_ = function (promise) {
  var self = this;

  if (this.nestedBuilders_) {
    return promise
      .thenReturn(Promise.all(self.nestedBuilders_))
      .then(_.flattenDeep);
  } else {
    return promise
      .thenReturn(self.knexQueryBuilder_)
      .then(function (rows) {
        return self.rowsToModels_(rows);
      });
  }
};

/**
 * @private
 * @param {Promise} promise
 * @returns {Promise}
 */
SqlModelQueryBuilder.prototype.executeFetchRelated_ = function (promise) {
  var self = this;
  if (this.eager_) {
    return promise.then(function (models) {
      return self.fetchRelated_(models);
    });
  } else {
    return promise;
  }
};

/**
 * @private
 * @param {{promise:Promise, resolve:function, reject:function}} pending
 * @param {Promise} promise
 * @returns {Promise}
 */
SqlModelQueryBuilder.prototype.executePending_ = function (pending, promise) {
  if (!pending) {
    return promise;
  }

  return promise.then(function (data) {
    pending.resolve(data);
    return pending.promise;
  });
};

/**
 * @private
 */
SqlModelQueryBuilder.prototype.rowsToModels_ = function (rows) {
  if (rows === null || rows === void 0 || rows.length === 0 || !_.isObject(rows[0])) {
    return rows;
  }

  for (var i = 0, l = rows.length; i < l; ++i) {
    rows[i] = this.modelClass_.fromDatabaseJson(rows[i]);
  }

  return rows;
};

/**
 * @private
 * @param {Array.<SqlModel>} models
 * @return {Promise|Array.<SqlModel>}
 */
SqlModelQueryBuilder.prototype.fetchRelated_ = function (models) {
  if (!_.isObject(models) || _.isEmpty(models)) {
    return models;
  }

  var self = this;
  var transaction = this.transaction_;
  var ModelClass = this.modelClass_;
  var namesOfRelationsToFetch = [];

  if (_.isString(this.eager_)) {
    namesOfRelationsToFetch = [this.eager_];
  } else if (_.isArray(this.eager_)) {
    namesOfRelationsToFetch = this.eager_;
  } else if (_.isObject(this.eager_)) {
    namesOfRelationsToFetch = _.keys(this.eager_);
  } else if (this.eager_ === SqlModelQueryBuilder.EagerType.AllRecursive) {
    namesOfRelationsToFetch = _.keys(ModelClass.getRelations());
  } else {
    return Promise.reject(new Error('invalid value for eager: ' + this.eager_));
  }

  // Fetch all relations and return a promise that is fulfilled after all fetches have finished.
  return Promise.all(_.map(namesOfRelationsToFetch, function (relationName) {
    var eager = _.clone(self.eager_);

    // Take the correct branch from the eager tree.
    if (_.isString(eager)) {
      eager = null;
    } else if (_.isArray(eager)) {
      eager = null;
    } else if (_.isObject(eager)) {
      eager = eager[relationName];
      // EagerType.Recursive means that we fetch the same relation recursively.
      if (eager === SqlModelQueryBuilder.EagerType.Recursive) {
        eager = {};
        eager[relationName] = SqlModelQueryBuilder.EagerType.Recursive;
      }
    }

    return ModelClass
      .findRelated(models, relationName)
      .transacting(transaction)
      .eager(eager);

  })).thenReturn(models);
};

/**
 * @param {object} modelClass
 * @returns {SqlModelQueryBuilder}
 */
SqlModelQueryBuilder.forClass = function (modelClass) {
  var builder = new SqlModelQueryBuilder();
  builder.modelClass_ = modelClass;
  builder.knexQueryBuilder_ = modelClass.dbQuery();
  return builder;
};

/**
 * @param {Array.<SqlModelQueryBuilder>} builders
 * @returns {SqlModelQueryBuilder}
 */
SqlModelQueryBuilder.all = function (builders) {
  var builder = new SqlModelQueryBuilder();
  builder.modelClass_ = builders.length ? builders[0].modelClass_ : null;

  _.each(builders, function (nestedBuilder) {
    if (nestedBuilder.modelClass_ !== builder.modelClass_) {
      throw new Error('SqlModelQueryBuilder.all: all builders must have the same model class');
    }
  });

  builder.nestedBuilders_ = builders;
  return builder;
};

/**
 * @param {*} value
 * @returns {SqlModelQueryBuilder}
 */
SqlModelQueryBuilder.returning = function (value) {
  return this.all([]).runAfterExecution(function () {
    return value;
  });
};

/**
 * Create a pass-through method to a bunch of knex.js methods.
 *
 * @note hand written for autocomplete.
 */

SqlModelQueryBuilder.prototype.select            = knexWrapper('select');
SqlModelQueryBuilder.prototype.columns           = knexWrapper('columns');
SqlModelQueryBuilder.prototype.column            = knexWrapper('column');
SqlModelQueryBuilder.prototype.from              = knexWrapper('from');
SqlModelQueryBuilder.prototype.into              = knexWrapper('into');
SqlModelQueryBuilder.prototype.withSchema        = knexWrapper('withSchema');
SqlModelQueryBuilder.prototype.table             = knexWrapper('table');
SqlModelQueryBuilder.prototype.columnInfo        = knexWrapper('columnInfo');
SqlModelQueryBuilder.prototype.distinct          = knexWrapper('distinct');
SqlModelQueryBuilder.prototype.join              = knexWrapper('join');
SqlModelQueryBuilder.prototype.innerJoin         = knexWrapper('innerJoin');
SqlModelQueryBuilder.prototype.leftJoin          = knexWrapper('leftJoin');
SqlModelQueryBuilder.prototype.leftOuterJoin     = knexWrapper('leftOuterJoin');
SqlModelQueryBuilder.prototype.rightJoin         = knexWrapper('rightJoin');
SqlModelQueryBuilder.prototype.rightOuterJoin    = knexWrapper('rightOuterJoin');
SqlModelQueryBuilder.prototype.outerJoin         = knexWrapper('outerJoin');
SqlModelQueryBuilder.prototype.fullOuterJoin     = knexWrapper('fullOuterJoin');
SqlModelQueryBuilder.prototype.crossJoin         = knexWrapper('crossJoin');
SqlModelQueryBuilder.prototype.joinRaw           = knexWrapper('joinRaw');
SqlModelQueryBuilder.prototype.where             = knexWrapper('where');
SqlModelQueryBuilder.prototype.andWhere          = knexWrapper('andWhere');
SqlModelQueryBuilder.prototype.orWhere           = knexWrapper('orWhere');
SqlModelQueryBuilder.prototype.whereNot          = knexWrapper('whereNot');
SqlModelQueryBuilder.prototype.andWhereNot       = knexWrapper('andWhereNot');
SqlModelQueryBuilder.prototype.orWhereNot        = knexWrapper('orWhereNot');
SqlModelQueryBuilder.prototype.whereRaw          = knexWrapper('whereRaw');
SqlModelQueryBuilder.prototype.whereWrapped      = knexWrapper('whereWrapped');
SqlModelQueryBuilder.prototype.andWhereRaw       = knexWrapper('andWhereRaw');
SqlModelQueryBuilder.prototype.orWhereRaw        = knexWrapper('orWhereRaw');
SqlModelQueryBuilder.prototype.whereExists       = knexWrapper('whereExists');
SqlModelQueryBuilder.prototype.orWhereExists     = knexWrapper('orWhereExists');
SqlModelQueryBuilder.prototype.whereNotExists    = knexWrapper('whereNotExists');
SqlModelQueryBuilder.prototype.orWhereNotExists  = knexWrapper('orWhereNotExists');
SqlModelQueryBuilder.prototype.whereIn           = knexWrapper('whereIn');
SqlModelQueryBuilder.prototype.orWhereIn         = knexWrapper('orWhereIn');
SqlModelQueryBuilder.prototype.whereNotIn        = knexWrapper('whereNotIn');
SqlModelQueryBuilder.prototype.orWhereNotIn      = knexWrapper('orWhereNotIn');
SqlModelQueryBuilder.prototype.whereNull         = knexWrapper('whereNull');
SqlModelQueryBuilder.prototype.orWhereNull       = knexWrapper('orWhereNull');
SqlModelQueryBuilder.prototype.whereNotNull      = knexWrapper('whereNotNull');
SqlModelQueryBuilder.prototype.orWhereNotNull    = knexWrapper('orWhereNotNull');
SqlModelQueryBuilder.prototype.whereBetween      = knexWrapper('whereBetween');
SqlModelQueryBuilder.prototype.whereNotBetween   = knexWrapper('whereNotBetween');
SqlModelQueryBuilder.prototype.orWhereBetween    = knexWrapper('orWhereBetween');
SqlModelQueryBuilder.prototype.orWhereNotBetween = knexWrapper('orWhereNotBetween');
SqlModelQueryBuilder.prototype.groupBy           = knexWrapper('groupBy');
SqlModelQueryBuilder.prototype.groupByRaw        = knexWrapper('groupByRaw');
SqlModelQueryBuilder.prototype.orderBy           = knexWrapper('orderBy');
SqlModelQueryBuilder.prototype.orderByRaw        = knexWrapper('orderByRaw');
SqlModelQueryBuilder.prototype.union             = knexWrapper('union');
SqlModelQueryBuilder.prototype.unionAll          = knexWrapper('unionAll');
SqlModelQueryBuilder.prototype.having            = knexWrapper('having');
SqlModelQueryBuilder.prototype.havingRaw         = knexWrapper('havingRaw');
SqlModelQueryBuilder.prototype.havingWrapped     = knexWrapper('havingWrapped');
SqlModelQueryBuilder.prototype.andHaving         = knexWrapper('andHaving');
SqlModelQueryBuilder.prototype.orHaving          = knexWrapper('orHaving');
SqlModelQueryBuilder.prototype.orHavingRaw       = knexWrapper('orHavingRaw');
SqlModelQueryBuilder.prototype.offset            = knexWrapper('offset');
SqlModelQueryBuilder.prototype.limit             = knexWrapper('limit');
SqlModelQueryBuilder.prototype.count             = knexWrapper('count');
SqlModelQueryBuilder.prototype.min               = knexWrapper('min');
SqlModelQueryBuilder.prototype.max               = knexWrapper('max');
SqlModelQueryBuilder.prototype.sum               = knexWrapper('sum');
SqlModelQueryBuilder.prototype.avg               = knexWrapper('avg');
SqlModelQueryBuilder.prototype.increment         = knexWrapper('increment');
SqlModelQueryBuilder.prototype.decrement         = knexWrapper('decrement');
SqlModelQueryBuilder.prototype.first             = knexWrapper('first');
SqlModelQueryBuilder.prototype.debug             = knexWrapper('debug');
SqlModelQueryBuilder.prototype.pluck             = knexWrapper('pluck');
SqlModelQueryBuilder.prototype.insert            = knexWrapper('insert');
SqlModelQueryBuilder.prototype.update            = knexWrapper('update');
SqlModelQueryBuilder.prototype.returning         = knexWrapper('returning');
SqlModelQueryBuilder.prototype.del               = knexWrapper('del');
SqlModelQueryBuilder.prototype.delete            = knexWrapper('delete');
SqlModelQueryBuilder.prototype.truncate          = knexWrapper('truncate');
SqlModelQueryBuilder.prototype.forUpdate         = knexWrapper('forUpdate');
SqlModelQueryBuilder.prototype.forShare          = knexWrapper('forShare');
SqlModelQueryBuilder.prototype.modify            = knexWrapper('modify');
SqlModelQueryBuilder.prototype.as                = knexWrapper('as');

function knexWrapper(knexMethod) {
  return function () {
    if (this.nestedBuilders_) {
      for (var i = 0, l = this.nestedBuilders_.length; i < l; ++i) {
        var nestedBuilder = this.nestedBuilders_[i];
        nestedBuilder[knexMethod].apply(nestedBuilder, arguments);
      }
    } else {
      this.knexQueryBuilder_[knexMethod].apply(this.knexQueryBuilder_, arguments);
    }
    return this;
  };
}

module.exports = SqlModelQueryBuilder;
